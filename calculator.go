package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"github.com/beevik/guid"
	"io"
	"net/http"
	"os"
	"sync"
)

var (
	KV = make(map[string]*Task, 0)
	Mu = &sync.Mutex{}
)

type Task struct {
	Url     string
	Status  string
	HashSum []byte
}

func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func Md5Hash(url string, guid string) {
	fmt.Println("url: " + url)
	fmt.Println("guid: " + guid)

	if err := DownloadFile(guid, url); err != nil {
		//panic(err)

		Mu.Lock()
		KV[guid].Status = "failure"
		Mu.Unlock()
		return
	}

	f, err := os.Open(guid)
	if err != nil {
		//log.Fatal(err)
		//panic(err)
		Mu.Lock()
		KV[guid].Status = "failure"
		Mu.Unlock()
		return
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		//log.Fatal(err)(
		//panic(err)
		Mu.Lock()
		KV[guid].Status = "failure"
		Mu.Unlock()
		return
	}

	Mu.Lock()
	KV[guid].HashSum = h.Sum(nil)
	KV[guid].Status = "done"
	fmt.Printf("KV(HashSum): %x\n", KV[guid].HashSum)
	Mu.Unlock()
	return
	//fmt.Printf("%x\n", h.Sum(nil))

}
func Submit(w http.ResponseWriter, r *http.Request) {

	var url string

	if r.Method == http.MethodPost {
		url = r.FormValue("url")
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	guid := guid.NewString()

	data := &struct {
		ID string `json:"id,string"`
	}{
		ID: guid,
	}

	//fmt.Println(data.ID)
	resp, err := json.Marshal(data)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w, "")
		return
	}

	w.WriteHeader(http.StatusOK)
	io.WriteString(w, string(resp))

	//fmt.Println(string(resp))
	Mu.Lock()
	KV[guid] = &Task{
		Url:     url,
		Status:  "running",
		HashSum: nil,
	}
	Mu.Unlock()

	go Md5Hash(url, guid)
}

func Check(w http.ResponseWriter, r *http.Request) {

	var id string

	if r.Method == http.MethodGet {
		id = r.URL.Query().Get("id")
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	task, existTask := KV[id]
	fmt.Println("Check(id): " + id)

	if existTask != true {
		// status = "notFound"

		data := &struct {
			Status string `json:"status"`
		}{
			Status: "notFound",
		}

		//fmt.Println(data.ID)
		resp, err := json.Marshal(data)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			io.WriteString(w, "")
			return
		}

		w.WriteHeader(http.StatusOK)
		io.WriteString(w, string(resp))
		return
	}
	if task.Status == "done" {
		// status + url + md5
		data := &struct {
			Status string `json:"status"`
			Md5    string `json:"md5"`
			Url    string `json:"url"`
		}{
			Status: "done",
			Md5:    fmt.Sprintf("%x", task.HashSum),
			Url:    task.Url,
		}

		fmt.Println(data.Md5)

		resp, err := json.Marshal(data)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			io.WriteString(w, "")
			return
		}

		w.WriteHeader(http.StatusOK)
		io.WriteString(w, string(resp))
		return
	} else {
		// status (running or failure)
		data := &struct {
			Status string `json:"status"`
		}{
			Status: task.Status,
		}

		//fmt.Println(data.ID)
		resp, err := json.Marshal(data)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			io.WriteString(w, "")
			return
		}

		w.WriteHeader(http.StatusOK)
		io.WriteString(w, string(resp))
		return
	}
}

func main() {

	http.HandleFunc("/submit", Submit)
	http.HandleFunc("/check", Check)

	fmt.Println("starting server at :1010")
	http.ListenAndServe(":1010", nil)

}
